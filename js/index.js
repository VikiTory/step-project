/* Section main-section */
const ul = document.querySelector('.tabs');

ul.addEventListener('click', (event) => {
    const tabsTarget = event.target.getAttribute('data-tab');
    const currentText = document.getElementById(tabsTarget);
    const activeTab = ul.querySelector(".active");
    const activeTabData = activeTab.getAttribute("data-tab");
    const activeText = document.getElementById(activeTabData);
    activeTab.classList.remove("active");
    activeText.classList.add("hidden-tabs-content");
    event.target.classList.add("active");
    currentText.classList.remove("hidden-tabs-content");
})

const workImg = document.querySelector('.work-img');
const workChoice = document.querySelector('.work-choice');

const data = [{url:"./images/graphic-design/graphic-design1.jpg", category:"Graphic Design"},{url:"./images/graphic-design/graphic-design2.jpg", category:"Graphic Design"},{url:"./images/graphic-design/graphic-design3.jpg",category:"Graphic Design"},
{url:"./images/graphic-design/graphic-design4.jpg", category:"Graphic Design"},{url:"./images/graphic-design/graphic-design5.jpg", category:"Graphic Design"},
{url:"./images/graphic-design/graphic-design6.jpg", category:"Graphic Design"},{url:"./images/web-design/web-design1.jpg", category:"Web Design"},{url:"./images/web-design/web-design2.jpg", category:"Web Design"},{url:"./images/web-design/web-design3.jpg",category:"Web Design"},
{url:"./images/web-design/web-design4.jpg", category:"Web Design"},{url:"./images/web-design/web-design5.jpg", category:"Web Design"},
{url:"./images/web-design/web-design6.jpg", category:"Web Design"},{url:"./images/landing-page/landing-page1.jpg", category:"Landing Pages"},{url:"./images/landing-page/landing-page2.jpg", category:"Landing Pages"},{url:"./images/landing-page/landing-page3.jpg",category:"Landing Pages"},
{url:"./images/landing-page/landing-page4.jpg", category:"Landing Pages"},{url:"./images/landing-page/landing-page5.jpg", category:"Landing Pages"},
{url:"./images/landing-page/landing-page6.jpg", category:"Landing Pages"},{url:"./images/wordpress/wordpress1.jpg", category:"Wordpress"},{url:"./images/wordpress/wordpress2.jpg", category:"Wordpress"},{url:"./images/wordpress/wordpress3.jpg",category:"Wordpress"},
{url:"./images/wordpress/wordpress4.jpg", category:"Wordpress"},{url:"./images/wordpress/wordpress5.jpg", category:"Wordpress"},
{url:"./images/wordpress/wordpress6.jpg", category:"Wordpress"}]
workChoice.addEventListener('click', (event) => {

    const tabsTarget = event.target.getAttribute('data-tab');
    const activeTab = workChoice.querySelector(".active");
    activeTab.classList.remove("active");
    event.target.classList.add("active");
    printCard(tabsTarget)
});
printCard("All");
function printCard(dataAtribut) {
     workImg.textContent = "";
     const filterCards = data.filter((objectCard, i) => {
       if (dataAtribut === "All" && i <= 11) {
        return true
       } else if(dataAtribut === "loadAll") {
        return true 
       } else {
        return objectCard.category === dataAtribut;
       }
      })
     filterCards.forEach(element => {

      workImg.insertAdjacentHTML("beforeend", `<div class="img-card"><img src="${element.url}" alt=""><div class="hidden-effect">
      <a class="effect" href="#">
          <svg width="15" height="15" viewBox="0 0 15 15" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"/>
          </svg>
      </a>
      <a class="effect" href="#">
          <svg width="12" height="11" viewBox="0 0 12 11" xmlns="http://www.w3.org/2000/svg">
              <rect width="12" height="11"/>
          </svg>
      </a>  
          <p class="creative-design">creative design</p>
          <p class="web-design">Web Design</p>
      
  </div></div>`)
    });
     if(dataAtribut === "All") {

     workImg.insertAdjacentHTML("beforeend", '<button class="load-btn"><img class="img-form" src="images/Forma 1.png" alt="">Load More</button>')
     const button = document.querySelector('.load-btn');
     button.addEventListener('click', () => {
           const AllTab = document.querySelector('[data-tab="All"]')
           AllTab.setAttribute("data-tab", "loadAll");
           printCard("loadAll")
     })
     }
}

$(document).ready(function(){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: true,
        dots: false,
        focusOnSelect: true,
        nextArrow: '<button class="slick-item"><svg class="arr-img h="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1.49213 0.48752L0.18704 1.88987L4.47503 6.49714L0.18704 11.1056L1.49213 12.5084L7.08508 6.49714L1.49213 0.48752Z" /></svg></button>',
        prevArrow: '<button class="slick-item"><svg class="arr-img" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M6.50764 12.5076L7.81261 11.1056L3.52463 6.49631L7.81261 1.88987L6.50764 0.486695L0.91457 6.49631L6.50764 12.5076Z" /></svg></button>',
        
    });
  });
                  
